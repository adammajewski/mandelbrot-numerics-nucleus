/*

I have an obsession with 1-file programs.


Here is an example :
how to compute nucleus of Mandelbrot set using Newton method
and mpc library 



Adam

------------------------------------------------
the code of this program is from library mandelbrot-numerics :
numerical algorithms related to the Mandelbrot set
by Claude Heiland-Allen


------------------------------------------------------------
Copyright: (C) Claude Heiland-Allen mathr.co.uk
License: http://www.gnu.org/licenses/gpl.html GPLv3+

http://code.mathr.co.uk/mandelbrot-numerics/blob_plain/HEAD:/c/lib/m_r_nucleus.c
http://code.mathr.co.uk/mandelbrot-numerics/blob/HEAD:/c/bin/m-nucleus.c
http://code.mathr.co.uk/mandelbrot-numerics/blob/HEAD:/c/bin/m-util.h
c/lib/m_d_nucleus.c
c/lib/m_d_util.h:14:static inline double cabs2(complex double z) {
---------------------------------------------------------------------

if it suceeds (maxiters not reached), the result is a nucleus with 
period dividing the input period (for example, if you start from c_guess 
= 0, you'll end up at c_final = 0 with period 1, even if you wanted 
period 7).

if maxiters is reached, the result might be close to a nucleus, but 
rounding errors or insufficient precision mean that the other stopping 
conditions aren't met.  or it could still be jumping around the fractal 
basins of attraction and be nowhere close to a nucleus.

> * one do not have to  check/adjust precision ?

a more advanced algorithm that adjusts precision appropriately can be 
made using this algorithm as a component.

see the muatom() code in mightymandel/src/atom/c (for example, which 
inlines this algorithm into part of the inner loop).

otherwise there are estimates for the precision needed - the smallest 
island of a given period (which I think might also be the smallest atom 
of that period) is the closest to -2, and their sizes follow a power law 
roughly  size = k / 16^period  and precision needed is related to log_2 
size.

see: http://www.mrob.com/pub/muency/r2tseries.html





----------------------



c console program 

gcc n.c  -std=c99 -lmpc -lmpfr -lgmp -lm -Wall
./a.out

./a.out double 1.0 0.0 1 100






*/


#include <stdio.h>
#include <errno.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>




static const double epsilon = 4.440892098500626e-16;
static const double epsilon2 = 1.9721522630525295e-31;// epsilon^2
static const double twopi = 6.283185307179586;


enum m_newton { m_failed, m_stepped, m_converged };
typedef enum m_newton m_newton;



static inline double cabs2(complex double z) {
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

static inline bool cisfinite(complex double z) {
  return isfinite(creal(z)) && isfinite(cimag(z));
}



/* ---------  arg ---------------------------------*/


static inline bool arg_precision(const char *arg, bool *native, int *bits) {
  if (0 == strcmp("double", arg)) {
    *native = true;
    *bits = 53;
    return true;
  } else {
    char *check = 0;
    errno = 0;
    long int li = strtol(arg, &check, 10);
    bool valid = ! errno && arg != check && ! *check;
    int i = li;
    if (valid && i > 1) {
      *native = false;
      *bits = i;
      return true;
    }
  }
  return false;
}

static inline bool arg_double(const char *arg, double *x) {
  char *check = 0;
  errno = 0;
  double d = strtod(arg, &check);
  if (! errno && arg != check && ! *check) {
    *x = d;
    return true;
  }
  return false;
}

static inline bool arg_int(const char *arg, int *x) {
  char *check = 0;
  errno = 0;
  long int li = strtol(arg, &check, 10);
  if (! errno && arg != check && ! *check) {
    *x = li;
    return true;
  }
  return false;
}

static inline bool arg_rational(const char *arg, mpq_t x) {
  int ok = mpq_set_str(x, arg, 10);
  mpq_canonicalize(x);
  return ok == 0;
}

static inline bool arg_mpfr(const char *arg, mpfr_t x) {
  return 0 == mpfr_set_str(x, arg, 10, MPFR_RNDN);
}

static inline bool arg_mpc(const char *re, const char *im, mpc_t x) {
  int ok
    = mpfr_set_str(mpc_realref(x), re, 10, MPFR_RNDN)
    + mpfr_set_str(mpc_imagref(x), im, 10, MPFR_RNDN);
  return ok == 0;
}



/* - --------------- using arbitrary precision -------------------- */
extern m_newton m_r_nucleus_step_raw(mpc_t c_out, const mpc_t c_guess, int period, mpc_t z, mpc_t dc, mpc_t c_new, mpc_t d, mpfr_t d2, mpfr_t epsilon2) {
  // z = 0; dc = 0;
  mpc_set_si(z, 0, MPC_RNDNN);
  mpc_set_si(dc, 0, MPC_RNDNN);
  for (int i = 0; i < period; ++i) {
    // dc = 2 * z * dc + 1;
    mpc_mul(dc, z, dc, MPC_RNDNN);
    mpc_mul_2si(dc, dc, 1, MPC_RNDNN);
    mpc_add_ui(dc, dc, 1, MPC_RNDNN);
    // z = z * z + c_guess;
    mpc_sqr(z, z, MPC_RNDNN);
    mpc_add(z, z, c_guess, MPC_RNDNN);
  }
  mpc_norm(d2, dc, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_converged;
  }
  // c_new = c_guess - z / dc;
  mpc_div(z, z, dc, MPC_RNDNN);
  mpc_sub(c_new, c_guess, z, MPC_RNDNN);
  // d = c_new - c_guess;
  mpc_sub(d, c_new, c_guess, MPC_RNDNN);
  mpc_norm(d2, d, MPFR_RNDN);
  if (mpfr_lessequal_p(d2, epsilon2)) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_converged;
  }
  if (mpfr_number_p(mpc_realref(d)) && mpfr_number_p(mpc_imagref(d))) {
    mpc_set(c_out, c_new, MPC_RNDNN);
    return m_stepped;
  } else {
    mpc_set(c_out, c_guess, MPC_RNDNN);
    return m_failed;
  }
}

extern m_newton m_r_nucleus_step(mpc_t c_out, const mpc_t c_guess, int period) {
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  mpc_set_prec(c_out, prec); // FIXME might trash when c_out = c_guess
  // init
  mpc_t z, dc, c_new, d;
  mpfr_t d2, epsilon2;
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // step raw
  m_newton retval = m_r_nucleus_step_raw(c_out, c_guess, period, z, dc, c_new, d, d2, epsilon2);
  // cleanup
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return retval;
}




extern m_newton m_r_nucleus(mpc_t c_out, const mpc_t c_guess, int period, int maxsteps) {
  m_newton result = m_failed;
  // prec
  mpfr_prec_t precr, preci, prec;
  mpc_get_prec2(&precr, &preci, c_guess);
  prec = precr > preci ? precr : preci;
  // init
  mpc_t c, z, dc, c_new, d;
  mpfr_t d2, epsilon2;
  mpc_init2(c, prec);
  mpc_init2(z, prec);
  mpc_init2(dc, prec);
  mpc_init2(c_new, prec);
  mpc_init2(d, prec);
  mpfr_init2(d2, prec);
  mpfr_init2(epsilon2, prec);
  // epsilon
  mpfr_set_si(epsilon2, 2, MPFR_RNDN);
  mpfr_nextabove(epsilon2);
  mpfr_sub_si(epsilon2, epsilon2, 2, MPFR_RNDN);
  mpfr_sqr(epsilon2, epsilon2, MPFR_RNDN);
  // c = c_guess
  mpc_set(c, c_guess, MPC_RNDNN);
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_r_nucleus_step_raw(c, c, period, z, dc, c_new, d, d2, epsilon2))) {
      break;
    }
  }
  // c_out = c;
  mpc_set_prec(c_out, prec);
  mpc_set(c_out, c, MPC_RNDNN);
  // cleanup
  mpc_clear(c);
  mpc_clear(z);
  mpc_clear(dc);
  mpc_clear(c_new);
  mpc_clear(d);
  mpfr_clear(d2);
  mpfr_clear(epsilon2);
  return result;
}


extern m_newton m_d_nucleus_step(complex double *c_out, complex double c_guess, int period) {
  complex double z = 0;
  complex double dc = 0;
  for (int i = 0; i < period; ++i) {
    dc = 2 * z * dc + 1;
    z = z * z + c_guess;
  }
  if (cabs2(dc) <= epsilon2) {
    *c_out = c_guess;
    return m_converged;
  }
  complex double c_new = c_guess - z / dc;
  complex double d = c_new - c_guess;
  if (cabs2(d) <= epsilon2) {
    *c_out = c_new;
    return m_converged;
  }
  if (cisfinite(d)) {
    *c_out = c_new;
    return m_stepped;
  } else {
    *c_out = c_guess;
    return m_failed;
  }
}

/* -------------- using double precision ---------------------------------*/
extern m_newton m_d_nucleus(complex double *c_out, complex double c_guess, int period, int maxsteps) {
  m_newton result = m_failed;
  complex double c = c_guess;
  for (int i = 0; i < maxsteps; ++i) {
    if (m_stepped != (result = m_d_nucleus_step(&c, c, period))) {
      break;
    }
  }
  *c_out = c;
  return result;
}


static void usage(const char *progname) {
  fprintf
    ( stderr
    , "usage: %s precision guess-re guess-im period maxsteps\n"
      " output re(nucleus) im(nucleus)\n" 
    , progname
    );
}




extern int main(int argc, char **argv) {
 

 if (argc != 6) {
    usage(argv[0]);
    return 1;    
  }


  bool native = true;
  int bits = 0;

  // compute and checks precision = bits 
  if (! arg_precision(argv[1], &native, &bits)) { return 1; }


  if (native) {
    // using double precision
    double cre = 0;
    double cim = 0;
    int period = 0;
    int maxsteps = 0;
    if (! arg_double(argv[2], &cre)) { return 1; }
    if (! arg_double(argv[3], &cim)) { return 1; }
    if (! arg_int(argv[4], &period)) { return 1; }
    if (! arg_int(argv[5], &maxsteps)) { return 1; }


    complex double c = 0;
    m_d_nucleus(&c, cre + I * cim, period, maxsteps);
    printf("%.16e %.16e using double\n", creal(c), cimag(c));
    return 0;

  } else { // using arbitrary precision mpc library 

    mpc_t c_guess;
    int period = 0;
    int maxsteps = 0;
    mpc_init2(c_guess, bits);
    if (! arg_mpc(argv[2], argv[3], c_guess)) { return 1; }
    if (! arg_int(argv[4], &period)) { return 1; }
    if (! arg_int(argv[5], &maxsteps)) { return 1; }
    mpc_t c_out;
    mpc_init2(c_out, bits);
    m_r_nucleus(c_out, c_guess, period, maxsteps);
    mpfr_printf("%Re %Re using mpc\n", mpc_realref(c_out), mpc_imagref(c_out));
    mpc_clear(c_out);
    mpc_clear(c_guess);
    return 0;
  }
  return 1;
}
